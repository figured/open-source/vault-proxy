#!/usr/bin/env bash

set -e

SCRIPT_SOURCE_URL="https://gitlab.com/figured/open-source/vault-proxy/-/raw/main/vault-proxy"
TARGET_BIN_DIR=/usr/local/bin
TARGET_SCRIPT_NAME=vault-proxy

if [[ "root" == $(whoami) ]]; then
    echo "Do not run this script as root!"
fi

if ! command -v sudo > /dev/null; then
    echo "Sudo not found. Aborting"
    exit 1
fi

if ! command -v socat > /dev/null; then
    echo "socat is not installed, attempting to install it with homebrew"
    if ! command -v brew > /dev/null; then
        echo "brew command is not found, giving up"
        exit 1
    fi

    brew install socat
fi

echo "Installing the $TARGET_SCRIPT_NAME script to '$TARGET_BIN_DIR/$TARGET_SCRIPT_NAME'. Your password will be requested"

sudo curl -q -o "$TARGET_BIN_DIR/$TARGET_SCRIPT_NAME" "$SCRIPT_SOURCE_URL" > /dev/null
sudo chmod 755 "$TARGET_BIN_DIR/$TARGET_SCRIPT_NAME"

echo "Install complete, you can run the proxy with '$TARGET_SCRIPT_NAME <boundaryProxiedUrlOrPort>'"
echo "If you get an error saying 'command not found: $TARGET_SCRIPT_NAME', you may need to add '$TARGET_BIN_DIR' to your path"
