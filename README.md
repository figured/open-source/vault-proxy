# Vault proxy for boundary

This project is a collection of scripts to help with using vault behind boundary.

## Requirements

* Unix like operating system
* bash
* socat
* optionally homebrew to install socat if it's not installed already

The scripts are written for MacOS and only tested on that!

## Installation

### Easy way with curl

```shell
/bin/bash -c "$( curl -fsSL https://gitlab.com/figured/open-source/vault-proxy/-/raw/main/install.sh)"
```

### Manual installation (advanced)

If you are uncomfortable running scripts directly from the internet (I don't blame you), the manual installation process 
is reasonably simple.

Just download the vault-proxy file from this repository save it in your path and set executable permissions on it 

## Running

```shell
vault-proxy <boundaryProxiedUrlOrPort>
```
